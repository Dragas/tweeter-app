package xyz.dragas.tweeter.azure;

import com.microsoft.azure.functions.*;
import com.microsoft.azure.functions.annotation.AuthorizationLevel;
import com.microsoft.azure.functions.annotation.FunctionName;
import com.microsoft.azure.functions.annotation.HttpTrigger;
import org.springframework.cloud.function.adapter.azure.AzureSpringBootRequestHandler;
import xyz.dragas.tweeter.api.model.User;
import xyz.dragas.tweeter.spring.model.TweetRequest;

import java.util.Optional;

public class TweetHandler extends AzureSpringBootRequestHandler {

    @FunctionName("tweetCreate")
    public HttpResponseMessage execute(
            @HttpTrigger(
                    name = "request",
                    methods = {HttpMethod.POST},
                    authLevel = AuthorizationLevel.ANONYMOUS
            ) HttpRequestMessage<Optional<TweetRequest>> request,
            ExecutionContext context
    ) {
        TweetRequest tweetRequest = request.getBody().orElseGet(TweetRequest::new);
        return request
                .createResponseBuilder(HttpStatus.OK)
                .body(handleRequest(tweetRequest, context))
                .header("Content-Type", "application/json")
                .build();
    }
}
