import {Component, Input, OnInit} from '@angular/core';
import {TweetControllerService} from "../api/services/tweet-controller.service";
import {UserService} from "../user.service";
import {TweetRequest} from "../api/models/tweet-request";
import {TweetResponse} from "../api/models/tweet-response";

@Component({
  selector: 'app-tweet-reply',
  templateUrl: './tweet-reply.component.html',
  styleUrls: ['./tweet-reply.component.scss']
})
export class TweetReplyComponent implements OnInit {
  private userService: UserService;
  private api: TweetControllerService;
  @Input()
  id?: number;
  request: TweetRequest;
  @Input()
  tweet: TweetResponse;

  constructor(api: TweetControllerService, userService: UserService) {
    this.api = api;
    this.userService = userService;
    this.request = {};
    this.id = 0;
    this.tweet = {};
  }

  ngOnInit(): void {
  }

  onSubmit($event: Event) {
    this.api.replyTweetUsingPost({
      "X-Authorization": this.userService.getToken(),
      id: this.id!,
      username: "@me",
      body: this.request
    }).subscribe((it) => {
      this.tweet.responses?.push(it);
      this.request = {};
    })
  }
}
