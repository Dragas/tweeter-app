# Tweeter app

Assignment implmentation

## Docker Setup

```bash
sudo ./setup_env.sh # adds infra references to hosts file
sudo sysctl -w vm.max_map_count=262144 # permits running elastic search
```
After setting this up infrastructure, like prometheus, grafana, etc. 
will be available under respective addresses

| Service            | URL                             |
|--------------------|---------------------------------|
| grafana            | http://grafana.infra            |
| kibana             | http://kibana.infra             |
| prometheus         | http://prometheus.infra         |
| tomcat-tweeter-app | http://tomcat-tweeter-app.infra |
| zookeeper          | http://zookeeper.infra          |

## Building tweeter-app

```bash
./build.sh
```

## Building tweeter-frontend

Note: this is under context of `node:14.17.0-slim` docker image
```bash
source setup-node.sh
```

## Running

### Only the tweeter-app
```bash
./run.sh
```
Note: URL will be http://tomcat-tweeter-app.infra:8080, since reverse proxy
is not running

### Entire infrastructure
```bash
docker-compose up
```