package xyz.dragas.tweeter.spring.security;


import org.springframework.security.core.userdetails.UserDetails;

import java.util.Optional;

public interface UserAuthenticationService {
    Optional<String> login(String username, String password);
    Optional<UserDetails> findByToken(String token);
    void logout(UserDetails user);
}
