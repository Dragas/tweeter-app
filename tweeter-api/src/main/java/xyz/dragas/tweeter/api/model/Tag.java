package xyz.dragas.tweeter.api.model;


public class Tag implements Comparable<Tag> {
    private String name;
    private String id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public int compareTo(Tag o) {
        if (o == null) {
            return 1;
        }
        return CharSequence.compare(getId(), o.getId());
    }
}
