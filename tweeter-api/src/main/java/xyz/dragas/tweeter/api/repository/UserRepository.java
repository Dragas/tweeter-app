package xyz.dragas.tweeter.api.repository;

import xyz.dragas.tweeter.api.model.User;

import java.util.Collection;

public interface UserRepository {

    User store(User user);

    void delete(User user);

    User get(String id);

    User getByUsername(String username);

    Collection<User> getAllUsers();

    Collection<User> getUsersByFragment(String username);
}
