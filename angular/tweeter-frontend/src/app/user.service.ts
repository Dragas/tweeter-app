import { Injectable } from '@angular/core';
import {UserResponse} from "./api/models/user-response";

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private token: string;
  private user: UserResponse;

  constructor() {
    this.token = "";
    this.user = {};
  }

  setToken(token: string) {
    this.token = token;
  }

  getToken() : string {
    return this.token;
  }

  setUser(user: UserResponse) {
    this.user = user;
  }

  getUser() : UserResponse {
    return this.user;
  }

  isLoggedIn(): boolean {
    return this.getToken() !== "";
  }
}
