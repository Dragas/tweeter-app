#!/usr/bin/env bash

az group create --name d_tweeter_rg --location northeurope
az storage account create --resource-group d_tweeter_rg --name dtweetersa
az appservice plan create --location northeurope --resource-group d_tweeter_rg --name d_tweeter_appplan --is-linux --sku B1
az functionapp create --name dtweeterappfrontfa --resource-group d_tweeter_rg --storage-account dtweetersa --functions-version 3 --plan d_tweeter_appplan --runtime java --runtime-version 11
az cosmosdb create --name dtweeterdb --resource-group d_tweeter_rg --locations regionName=northeurope --kind MongoD
az extension add --name spring-cloud
#az spring-cloud create -n dtweeterspringcloud -g d_tweeter_rg -l northeurope
#az spring-cloud app create --name dtweeterspringapp -s dtweeterspringcloud --resource-group d_tweeter_rg --assign-endpoint true --runtime-version=Java_11
az acr create -n dtweeterregistry -g d_tweeter_rg --sku standard
az acr login --name dtweeterregistry
docker login azure
docker context create aci dtweetercontext
docker context use dtweetercontext
docker volume create dtweetervolume --storage-account dtweetersa
az storage directory create -n prometheus -s dtweetervolume --account-name dtweetersa
az storage file upload -s dtweetervolume --source prometheus/prometheus.yml --path prometheus/prometheus.yml --account-name dtweetersa

docker volume create dtweetergrafanadb --storage-account dtweetersa
docker volume create dtweetergrafanads --storage-account dtweetersa
docker volume create dtweetergrafana --storage-account dtweetersa
az storage file upload -s dtweetergrafanadb --source grafana/grafana-jvm-memory-db.yml --account-name dtweetersa
az storage file upload -s dtweetergrafanads --source grafana/grafana-prometheus-ds.yml --account-name dtweetersa
az storage file upload-batch --destination dtweetergrafana --source grafana/dashboards --destination-path . --account-name dtweetersa

az storage directory create -n elk -s dtweetervolume --account-name dtweetersa
az storage directory create -n elk/modules.d -s dtweetervolume --account-name dtweetersa
az storage directory create -n elk/pipelines -s dtweetervolume --account-name dtweetersa

docker volume create dtweetervolumekafka --storage-account dtweetersa
az storage file upload-batch --destination dtweetervolumekafka --source kafka --destination-path . --account-name dtweetersa

docker volume create dtweetervolumezookeeper --storage-account dtweetersa
az storage file upload-batch --destination dtweetervolumezookeeper --source zookeeper --destination-path . --account-name dtweetersa

docker volume create dtweeterkibana --storage-account dtweetersa
az storage file upload -s dtweeterkibana --source elk/kibana.yml --account-name dtweetersa
docker volume create dtweeterlogstash --storage-account dtweetersa
az storage file upload -s dtweeterlogstash --source elk/logstash.yml --account-name dtweetersa
docker volume create dtweeterlogstashpipelines --storage-account dtweetersa
az storage file upload -s dtweeterlogstashpipelines --source elk/pipelines/tweeter-app.conf --account-name dtweetersa

docker compose up