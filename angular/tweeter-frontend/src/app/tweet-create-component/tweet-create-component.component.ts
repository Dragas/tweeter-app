import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {TweetRequest} from "../api/models/tweet-request";
import {TweetControllerService} from "../api/services/tweet-controller.service";
import {UserService} from "../user.service";
import {TweetResponse} from "../api/models/tweet-response";
import {TweetErrorResponse} from "../api/models/tweet-error-response";

@Component({
  selector: 'app-tweet-create-component',
  templateUrl: './tweet-create-component.component.html',
  styleUrls: ['./tweet-create-component.component.scss']
})
export class TweetCreateComponentComponent implements OnInit {
  tweetRequest: TweetRequest;
  private api: TweetControllerService;
  private userService: UserService;
  @Output()
  tweetCreate: EventEmitter<TweetResponse> = new EventEmitter<TweetResponse>()
  error: TweetErrorResponse;

  constructor(api: TweetControllerService, userService: UserService) {
    this.tweetRequest = this.createTweetRequest();
    this.error = this.createTweetErrorResponse();
    this.api = api;
    this.userService = userService;
  }

  private createTweetRequest(): TweetRequest {
    return {
      message: "",
      tag: ""
    };
  }

  ngOnInit(): void {
  }

  onSubmit($event: Event) {
    this.error = {};
    this.api.createTweetUsingPost({
      "X-Authorization": this.userService.getToken(),
      username: "@me",
      body: this.tweetRequest
    })
      .subscribe((it) => {
        this.tweetRequest = this.createTweetRequest();
        this.tweetCreate.emit(it);
      }, (error) => {
        this.error = error.error;
      })
  }

  private createTweetErrorResponse(): TweetErrorResponse {
    return {
      message: [],
      tag: []
    };
  }
}
