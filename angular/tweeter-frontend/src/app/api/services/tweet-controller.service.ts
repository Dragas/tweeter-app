/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { LoginUserRequest } from '../models/login-user-request';
import { RegisterUserRequest } from '../models/register-user-request';
import { TweetRequest } from '../models/tweet-request';
import { TweetResponse } from '../models/tweet-response';
import { UserResponse } from '../models/user-response';


/**
 * Tweet Controller
 */
@Injectable({
  providedIn: 'root',
})
export class TweetControllerService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation getAllTweetsUsingGet
   */
  static readonly GetAllTweetsUsingGetPath = '/api/v1.0/tweets/all';

  /**
   * getAllTweets.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getAllTweetsUsingGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  getAllTweetsUsingGet$Response(params: {
    'X-Authorization': string;
  }): Observable<StrictHttpResponse<Array<TweetResponse>>> {

    const rb = new RequestBuilder(this.rootUrl, TweetControllerService.GetAllTweetsUsingGetPath, 'get');
    if (params) {
      rb.header('X-Authorization', params['X-Authorization'], {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Array<TweetResponse>>;
      })
    );
  }

  /**
   * getAllTweets.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `getAllTweetsUsingGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getAllTweetsUsingGet(params: {
    'X-Authorization': string;
  }): Observable<Array<TweetResponse>> {

    return this.getAllTweetsUsingGet$Response(params).pipe(
      map((r: StrictHttpResponse<Array<TweetResponse>>) => r.body as Array<TweetResponse>)
    );
  }

  /**
   * Path part for operation loginUsingPost
   */
  static readonly LoginUsingPostPath = '/api/v1.0/tweets/login';

  /**
   * login.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `loginUsingPost()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  loginUsingPost$Response(params?: {
    body?: LoginUserRequest
  }): Observable<StrictHttpResponse<string>> {

    const rb = new RequestBuilder(this.rootUrl, TweetControllerService.LoginUsingPostPath, 'post');
    if (params) {
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<string>;
      })
    );
  }

  /**
   * login.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `loginUsingPost$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  loginUsingPost(params?: {
    body?: LoginUserRequest
  }): Observable<string> {

    return this.loginUsingPost$Response(params).pipe(
      map((r: StrictHttpResponse<string>) => r.body as string)
    );
  }

  /**
   * Path part for operation registerUserUsingPost
   */
  static readonly RegisterUserUsingPostPath = '/api/v1.0/tweets/register';

  /**
   * registerUser.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `registerUserUsingPost()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  registerUserUsingPost$Response(params?: {
    body?: RegisterUserRequest
  }): Observable<StrictHttpResponse<UserResponse>> {

    const rb = new RequestBuilder(this.rootUrl, TweetControllerService.RegisterUserUsingPostPath, 'post');
    if (params) {
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<UserResponse>;
      })
    );
  }

  /**
   * registerUser.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `registerUserUsingPost$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  registerUserUsingPost(params?: {
    body?: RegisterUserRequest
  }): Observable<UserResponse> {

    return this.registerUserUsingPost$Response(params).pipe(
      map((r: StrictHttpResponse<UserResponse>) => r.body as UserResponse)
    );
  }

  /**
   * Path part for operation getUsersUsingGet
   */
  static readonly GetUsersUsingGetPath = '/api/v1.0/tweets/user/search/{username}';

  /**
   * getUsers.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getUsersUsingGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  getUsersUsingGet$Response(params: {
    'X-Authorization': string;

    /**
     * username
     */
    username: string;
  }): Observable<StrictHttpResponse<Array<UserResponse>>> {

    const rb = new RequestBuilder(this.rootUrl, TweetControllerService.GetUsersUsingGetPath, 'get');
    if (params) {
      rb.header('X-Authorization', params['X-Authorization'], {});
      rb.path('username', params.username, {"style":"simple"});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Array<UserResponse>>;
      })
    );
  }

  /**
   * getUsers.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `getUsersUsingGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getUsersUsingGet(params: {
    'X-Authorization': string;

    /**
     * username
     */
    username: string;
  }): Observable<Array<UserResponse>> {

    return this.getUsersUsingGet$Response(params).pipe(
      map((r: StrictHttpResponse<Array<UserResponse>>) => r.body as Array<UserResponse>)
    );
  }

  /**
   * Path part for operation getUsersUsingGet1
   */
  static readonly GetUsersUsingGet1Path = '/api/v1.0/tweets/users/all';

  /**
   * getUsers.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getUsersUsingGet1()` instead.
   *
   * This method doesn't expect any request body.
   */
  getUsersUsingGet1$Response(params: {
    'X-Authorization': string;
  }): Observable<StrictHttpResponse<Array<UserResponse>>> {

    const rb = new RequestBuilder(this.rootUrl, TweetControllerService.GetUsersUsingGet1Path, 'get');
    if (params) {
      rb.header('X-Authorization', params['X-Authorization'], {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Array<UserResponse>>;
      })
    );
  }

  /**
   * getUsers.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `getUsersUsingGet1$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getUsersUsingGet1(params: {
    'X-Authorization': string;
  }): Observable<Array<UserResponse>> {

    return this.getUsersUsingGet1$Response(params).pipe(
      map((r: StrictHttpResponse<Array<UserResponse>>) => r.body as Array<UserResponse>)
    );
  }

  /**
   * Path part for operation getTweetsUsingGet
   */
  static readonly GetTweetsUsingGetPath = '/api/v1.0/tweets/{username}';

  /**
   * getTweets.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getTweetsUsingGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  getTweetsUsingGet$Response(params: {
    'X-Authorization': string;

    /**
     * username
     */
    username: string;
  }): Observable<StrictHttpResponse<Array<TweetResponse>>> {

    const rb = new RequestBuilder(this.rootUrl, TweetControllerService.GetTweetsUsingGetPath, 'get');
    if (params) {
      rb.header('X-Authorization', params['X-Authorization'], {});
      rb.path('username', params.username, {"style":"simple"});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Array<TweetResponse>>;
      })
    );
  }

  /**
   * getTweets.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `getTweetsUsingGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getTweetsUsingGet(params: {
    'X-Authorization': string;

    /**
     * username
     */
    username: string;
  }): Observable<Array<TweetResponse>> {

    return this.getTweetsUsingGet$Response(params).pipe(
      map((r: StrictHttpResponse<Array<TweetResponse>>) => r.body as Array<TweetResponse>)
    );
  }

  /**
   * Path part for operation createTweetUsingPost
   */
  static readonly CreateTweetUsingPostPath = '/api/v1.0/tweets/{username}/add';

  /**
   * createTweet.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `createTweetUsingPost()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  createTweetUsingPost$Response(params: {
    'X-Authorization': string;
    username: any;
    body?: TweetRequest
  }): Observable<StrictHttpResponse<TweetResponse>> {

    const rb = new RequestBuilder(this.rootUrl, TweetControllerService.CreateTweetUsingPostPath, 'post');
    if (params) {
      rb.header('X-Authorization', params['X-Authorization'], {});
      rb.path('username', params.username, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<TweetResponse>;
      })
    );
  }

  /**
   * createTweet.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `createTweetUsingPost$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  createTweetUsingPost(params: {
    'X-Authorization': string;
    username: any;
    body?: TweetRequest
  }): Observable<TweetResponse> {

    return this.createTweetUsingPost$Response(params).pipe(
      map((r: StrictHttpResponse<TweetResponse>) => r.body as TweetResponse)
    );
  }

  /**
   * Path part for operation deleteTweetUsingDelete
   */
  static readonly DeleteTweetUsingDeletePath = '/api/v1.0/tweets/{username}/delete/{id}';

  /**
   * deleteTweet.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `deleteTweetUsingDelete()` instead.
   *
   * This method doesn't expect any request body.
   */
  deleteTweetUsingDelete$Response(params: {
    'X-Authorization': string;

    /**
     * id
     */
    id: number;
    username: any;
  }): Observable<StrictHttpResponse<TweetResponse>> {

    const rb = new RequestBuilder(this.rootUrl, TweetControllerService.DeleteTweetUsingDeletePath, 'delete');
    if (params) {
      rb.header('X-Authorization', params['X-Authorization'], {});
      rb.path('id', params.id, {"style":"simple"});
      rb.path('username', params.username, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<TweetResponse>;
      })
    );
  }

  /**
   * deleteTweet.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `deleteTweetUsingDelete$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  deleteTweetUsingDelete(params: {
    'X-Authorization': string;

    /**
     * id
     */
    id: number;
    username: any;
  }): Observable<TweetResponse> {

    return this.deleteTweetUsingDelete$Response(params).pipe(
      map((r: StrictHttpResponse<TweetResponse>) => r.body as TweetResponse)
    );
  }

  /**
   * Path part for operation likeTweetUsingPut
   */
  static readonly LikeTweetUsingPutPath = '/api/v1.0/tweets/{username}/like/{id}';

  /**
   * likeTweet.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `likeTweetUsingPut()` instead.
   *
   * This method doesn't expect any request body.
   */
  likeTweetUsingPut$Response(params: {
    'X-Authorization': string;

    /**
     * id
     */
    id: number;
    username: any;
  }): Observable<StrictHttpResponse<TweetResponse>> {

    const rb = new RequestBuilder(this.rootUrl, TweetControllerService.LikeTweetUsingPutPath, 'put');
    if (params) {
      rb.header('X-Authorization', params['X-Authorization'], {});
      rb.path('id', params.id, {"style":"simple"});
      rb.path('username', params.username, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<TweetResponse>;
      })
    );
  }

  /**
   * likeTweet.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `likeTweetUsingPut$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  likeTweetUsingPut(params: {
    'X-Authorization': string;

    /**
     * id
     */
    id: number;
    username: any;
  }): Observable<TweetResponse> {

    return this.likeTweetUsingPut$Response(params).pipe(
      map((r: StrictHttpResponse<TweetResponse>) => r.body as TweetResponse)
    );
  }

  /**
   * Path part for operation replyTweetUsingPost
   */
  static readonly ReplyTweetUsingPostPath = '/api/v1.0/tweets/{username}/reply/{id}';

  /**
   * replyTweet.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `replyTweetUsingPost()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  replyTweetUsingPost$Response(params: {
    'X-Authorization': string;

    /**
     * id
     */
    id: number;
    username: any;
    body?: TweetRequest
  }): Observable<StrictHttpResponse<TweetResponse>> {

    const rb = new RequestBuilder(this.rootUrl, TweetControllerService.ReplyTweetUsingPostPath, 'post');
    if (params) {
      rb.header('X-Authorization', params['X-Authorization'], {});
      rb.path('id', params.id, {"style":"simple"});
      rb.path('username', params.username, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<TweetResponse>;
      })
    );
  }

  /**
   * replyTweet.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `replyTweetUsingPost$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  replyTweetUsingPost(params: {
    'X-Authorization': string;

    /**
     * id
     */
    id: number;
    username: any;
    body?: TweetRequest
  }): Observable<TweetResponse> {

    return this.replyTweetUsingPost$Response(params).pipe(
      map((r: StrictHttpResponse<TweetResponse>) => r.body as TweetResponse)
    );
  }

  /**
   * Path part for operation updateTweetUsingPut
   */
  static readonly UpdateTweetUsingPutPath = '/api/v1.0/tweets/{username}/update/{id}';

  /**
   * updateTweet.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `updateTweetUsingPut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  updateTweetUsingPut$Response(params: {
    'X-Authorization': string;

    /**
     * id
     */
    id: number;
    username: any;
    body?: string
  }): Observable<StrictHttpResponse<TweetResponse>> {

    const rb = new RequestBuilder(this.rootUrl, TweetControllerService.UpdateTweetUsingPutPath, 'put');
    if (params) {
      rb.header('X-Authorization', params['X-Authorization'], {});
      rb.path('id', params.id, {"style":"simple"});
      rb.path('username', params.username, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<TweetResponse>;
      })
    );
  }

  /**
   * updateTweet.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `updateTweetUsingPut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  updateTweetUsingPut(params: {
    'X-Authorization': string;

    /**
     * id
     */
    id: number;
    username: any;
    body?: string
  }): Observable<TweetResponse> {

    return this.updateTweetUsingPut$Response(params).pipe(
      map((r: StrictHttpResponse<TweetResponse>) => r.body as TweetResponse)
    );
  }

}
