package xyz.dragas.tweeter.azure;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import xyz.dragas.tweeter.api.service.TweetService;
import xyz.dragas.tweeter.spring.model.TweetRequest;
import xyz.dragas.tweeter.spring.model.TweetResponse;
import xyz.dragas.tweeter.spring.service.TweetManagementService;

import java.util.Collection;
import java.util.function.Function;

@Component
public class TweetCreateFunction implements Function<TweetRequest, TweetResponse> {

    private final TweetManagementService tweetService;

    public TweetCreateFunction(TweetManagementService tweetService) {
        this.tweetService = tweetService;
    }

    @Override
    public TweetResponse apply(TweetRequest request) {
        return tweetService.createTweet(new UserDetails() {
            @Override
            public Collection<? extends GrantedAuthority> getAuthorities() {
                return null;
            }

            @Override
            public String getPassword() {
                return null;
            }

            @Override
            public String getUsername() {
                return "";
            }

            @Override
            public boolean isAccountNonExpired() {
                return false;
            }

            @Override
            public boolean isAccountNonLocked() {
                return false;
            }

            @Override
            public boolean isCredentialsNonExpired() {
                return false;
            }

            @Override
            public boolean isEnabled() {
                return false;
            }
        }, request);
    }
}
