package xyz.dragas.tweeter.core.utility;

import xyz.dragas.tweeter.api.model.User;

import java.util.function.Predicate;

public class UsernamePredicate implements Predicate<User> {

    private final String username;

    public UsernamePredicate(String name) {
        this.username = name;
    }

    @Override
    public boolean test(User user) {
        return user.getLoginId().equals(getUsername());
    }

    public String getUsername() {
        return username;
    }
}
