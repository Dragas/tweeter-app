package xyz.dragas.tweeter.mongo;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import xyz.dragas.tweeter.api.model.Tag;
import xyz.dragas.tweeter.api.repository.TagRepository;

import java.util.Collection;

public class MongoTagRepository implements TagRepository {
    private final MongoTemplate mongoClient;

    public MongoTagRepository(MongoTemplate mongoTemplate) {
        this.mongoClient = mongoTemplate;
    }

    @Override
    public Tag store(Tag tag) {
        return mongoClient.insert(tag, "tags");
    }

    @Override
    public void delete(Tag tag) {
        mongoClient.findAndRemove(
                Query.query(Criteria.where("_id").is(tag.getId())), Tag.class, "tags"
        );
    }

    @Override
    public Tag get(String id) {
        return mongoClient.findOne(
                Query.query(Criteria.where("_id").is(id)), Tag.class, "tags"
        );
    }

    @Override
    public Collection<Tag> getByNamePart(String name) {
        return mongoClient.find(
                Query.query(Criteria.where("name").is(name)), Tag.class, "tags"
        );
    }
}
