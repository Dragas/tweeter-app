package xyz.dragas.tweeter.api.service;

import xyz.dragas.tweeter.api.model.Tag;

import java.util.Collection;

public interface TagService {

    Tag createTag(Tag tag);

    Tag getTag(String id);

    Collection<Tag> getTagsByNamePart(String name);

}
