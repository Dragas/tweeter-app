package xyz.dragas.tweeter.core.service;

import xyz.dragas.tweeter.api.model.Tweet;
import xyz.dragas.tweeter.api.model.User;
import xyz.dragas.tweeter.api.repository.UserRepository;
import xyz.dragas.tweeter.api.service.UserService;

import java.util.Collection;

public class DefaultUserService implements UserService {
    private final UserRepository userRepostory;

    public DefaultUserService(UserRepository userRepository) {
        this.userRepostory = userRepository;
    }

    @Override
    public User createUser(User user) {
        return userRepostory.store(user);
    }

    @Override
    public User getUser(String id) {
        return userRepostory.get(id);
    }

    @Override
    public User getUserByUsername(String username) {
        return userRepostory.getByUsername(username);
    }

    @Override
    public Collection<User> getUsers() {
        return userRepostory.getAllUsers();
    }

    @Override
    public Collection<User> getUsers(String username) {
        return userRepostory.getUsersByFragment(username);
    }

    @Override
    public void likeTweet(User user, Tweet tweet) {
        user.getLikedTweets().add(tweet);
        userRepostory.store(user);
    }
}
