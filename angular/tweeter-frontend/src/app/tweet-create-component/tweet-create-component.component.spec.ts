import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TweetCreateComponentComponent } from './tweet-create-component.component';

describe('TweetCreateComponentComponent', () => {
  let component: TweetCreateComponentComponent;
  let fixture: ComponentFixture<TweetCreateComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TweetCreateComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TweetCreateComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
