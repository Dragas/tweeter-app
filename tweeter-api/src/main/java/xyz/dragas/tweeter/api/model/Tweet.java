package xyz.dragas.tweeter.api.model;

import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.TreeSet;

public class Tweet implements Comparable<Tweet> {

    private User author;
    private String parent;
    private Collection<Tweet> responses = new TreeSet<>();
    private ZonedDateTime createdAt;
    private String message;
    private String id;
    private Tag tag;

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    @Override
    public int compareTo(Tweet o) {
        if(o == null || o.getId() == null) {
            return 1;
        }
        if(getId() == null) {
            return -1;
        }
        return CharSequence.compare(getId(), o.getId());
    }

    public Collection<Tweet> getResponses() {
        return responses;
    }

    public void setResponses(Collection<Tweet> responses) {
        this.responses = responses;
    }

    public void setTag(Tag tag) {
        this.tag = tag;
    }

    public Tag getTag() {
        return tag;
    }
}
