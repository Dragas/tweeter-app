package xyz.dragas.tweeter.core.service;

import xyz.dragas.tweeter.api.model.Tag;
import xyz.dragas.tweeter.api.repository.TagRepository;
import xyz.dragas.tweeter.api.service.TagService;

import java.util.Collection;

public class DefaultTagService implements TagService {

    private final TagRepository tagRepository;

    public DefaultTagService(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    @Override
    public Tag createTag(Tag tag) {
        return tagRepository.store(tag);
    }

    @Override
    public Tag getTag(String id) {
        return tagRepository.get(id);
    }

    @Override
    public Collection<Tag> getTagsByNamePart(String name) {
        return tagRepository.getByNamePart(name);
    }
}
