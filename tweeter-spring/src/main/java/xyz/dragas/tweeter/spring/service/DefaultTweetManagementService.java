package xyz.dragas.tweeter.spring.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import xyz.dragas.tweeter.api.model.Tag;
import xyz.dragas.tweeter.api.model.Tweet;
import xyz.dragas.tweeter.api.model.User;
import xyz.dragas.tweeter.api.service.TagService;
import xyz.dragas.tweeter.api.service.TweetService;
import xyz.dragas.tweeter.api.service.UserService;
import xyz.dragas.tweeter.spring.mapper.TweetMapper;
import xyz.dragas.tweeter.spring.model.TweetRequest;
import xyz.dragas.tweeter.spring.model.TweetResponse;

import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

@Service
public class DefaultTweetManagementService implements TweetManagementService {

    private final TweetMapper tweetMapper;
    private final TweetService tweetService;
    private final UserService userService;
    private final TagService tagService;

    public DefaultTweetManagementService(
            TweetService tweetService,
            TweetMapper tweetMapper,
            UserService userService,
            TagService tagService
    ) {
        this.tweetService = tweetService;
        this.tweetMapper = tweetMapper;
        this.userService = userService;
        this.tagService = tagService;
    }


    @Override
    public Collection<TweetResponse> getTweets() {
        return tweetService.getTweets().stream().map(tweetMapper::toResponse).collect(Collectors.toUnmodifiableSet());
    }

    @Override
    public Collection<TweetResponse> getTweets(String username) {
        User u = userService.getUserByUsername(username);
        if(u == null) {
            return Collections.emptyList();
        }
        return tweetService.getTweets(u).stream().map(tweetMapper::toResponse).collect(Collectors.toUnmodifiableSet());
    }

    @Override
    public TweetResponse createTweet(UserDetails details, TweetRequest request) {
        User u = userService.getUserByUsername(details.getUsername());
        Tweet tweet = tweetMapper.toTweet(request);
        tweet.setAuthor(u);
        tweet = tweetService.createTweet(tweet);
        if(request.getTag() != null) {
            Tag tag = new Tag();
            tag.setName(request.getTag());
            tagService.createTag(tag);
            tweetService.associateTag(tweet, tag);
        }
        return tweetMapper.toResponse(tweet);
    }

    @Override
    public TweetResponse updateTweet(UserDetails details, String id, String content) {
        Tweet tweet = tweetService.getTweet(id);
        if(tweet == null) {
            return null;
        }
        if(!tweet.getAuthor().getLoginId().equals(details.getUsername())) {
            return null;
        }
        tweetService.updateContent(tweet, content);
        return tweetMapper.toResponse(tweet);
    }

    @Override
    public TweetResponse deleteTweet(UserDetails details, String id) {
        Tweet tweet = tweetService.getTweet(id);
        if(tweet == null) {
            return null;
        }
        if(!tweet.getAuthor().getLoginId().equals(details.getUsername())) {
            return null;
        }
        tweetService.deleteTweet(tweet);
        return new TweetResponse();
    }

    @Override
    public TweetResponse likeTweet(UserDetails details, String id) {
        Tweet tweet = tweetService.getTweet(id);
        if(tweet == null) {
            return null;
        }
        User user = userService.getUserByUsername(details.getUsername());
        userService.likeTweet(user, tweet);
        return tweetMapper.toResponse(tweet);
    }

    @Override
    public TweetResponse replyTweet(UserDetails details, String id, TweetRequest request) {
        Tweet tweet = tweetService.getTweet(id);
        if(tweet == null) {
            return null;
        }
        TweetResponse response = createTweet(details, request);
        Tweet reply = tweetService.getTweet(response.getId());
        tweetService.replyToTweet(tweet, reply);
        return tweetMapper.toResponse(reply);
    }
}
