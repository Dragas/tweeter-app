package xyz.dragas.tweeter.spring.model;

import java.util.Collection;
import java.util.TreeSet;

public class UserResponse implements Comparable<UserResponse>{
    private String username;
    private Collection<String> likedTweets = new TreeSet<String>();

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public int compareTo(UserResponse o) {
        return getUsername().compareTo(o.getUsername());
    }

    public Collection<String> getLikedTweets() {
        return likedTweets;
    }

    public void setLikedTweets(Collection<String> likedTweets) {
        this.likedTweets = likedTweets;
    }
}
