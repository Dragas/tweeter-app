/* tslint:disable */
/* eslint-disable */
export interface TweetResponse {
  author?: string;
  createdAt?: string;
  id?: number;
  message?: string;
  responses?: Array<TweetResponse>;
  tag?: string;
}
