package xyz.dragas.tweeter.spring.mapper;

import xyz.dragas.tweeter.api.model.Tag;
import xyz.dragas.tweeter.api.model.Tweet;
import xyz.dragas.tweeter.spring.model.TweetRequest;
import xyz.dragas.tweeter.spring.model.TweetResponse;

import java.util.stream.Collectors;

public class TweetMapper {

    public TweetResponse toResponse(Tweet tweet) {
        TweetResponse tweetResponse = new TweetResponse();
        tweetResponse.setAuthor(tweet.getAuthor().getLoginId());
        tweetResponse.setMessage(tweet.getMessage());
        tweetResponse.setResponses(tweet.getResponses().stream().map(this::toResponse).collect(Collectors.toUnmodifiableSet()));
        tweetResponse.setCreatedAt(tweet.getCreatedAt());
        tweetResponse.setId(tweet.getId());
        final Tag tag = tweet.getTag();
        if(tag != null) {
            tweetResponse.setTag(tag.getName());
        }
        return tweetResponse;
    }

    public Tweet toTweet(TweetRequest tweetRequest) {
        Tweet tweet = new Tweet();
        tweet.setMessage(tweetRequest.getMessage());
        return tweet;
    }
}
