package xyz.dragas.tweeter.spring.security;

import org.springframework.security.core.userdetails.UserDetails;

import java.util.UUID;

public interface AuthenticationRepository {

    UserDetails getUserDetails(String username);

    String getTokenForUser(String username);

    UserDetails getUserByToken(String uuid);

    void logout(UserDetails user);
}
