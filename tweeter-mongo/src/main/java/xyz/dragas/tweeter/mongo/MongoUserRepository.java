package xyz.dragas.tweeter.mongo;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import xyz.dragas.tweeter.api.model.User;
import xyz.dragas.tweeter.api.repository.UserRepository;

import java.util.Collection;

public class MongoUserRepository implements UserRepository {

    private final MongoTemplate mongoClient;

    public MongoUserRepository(MongoTemplate mongoTemplate) {
        this.mongoClient = mongoTemplate;
    }

    @Override
    public User store(User user) {
        return mongoClient.insert(user, "users");
    }

    @Override
    public void delete(User user) {
        mongoClient.findAllAndRemove(Query.query(Criteria.where("_id").is(user.getId())), User.class, "users");
    }

    @Override
    public User get(String id) {
        return mongoClient.findOne(Query.query(Criteria.where("_id").is(id)), User.class, "users");
    }

    @Override
    public User getByUsername(String username) {
        return mongoClient.findOne(Query.query(Criteria.where("loginId").is(username)), User.class, "users");
    }

    @Override
    public Collection<User> getAllUsers() {
        return mongoClient.findAll(User.class, "users");
    }

    @Override
    public Collection<User> getUsersByFragment(String username) {
        return mongoClient.find(Query.query(Criteria.where("loginId").regex(String.format(".*%s.*", username))), User.class, "users");
    }
}
