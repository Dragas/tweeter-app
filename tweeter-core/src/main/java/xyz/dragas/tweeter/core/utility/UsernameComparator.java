package xyz.dragas.tweeter.core.utility;

import xyz.dragas.tweeter.api.model.User;

import java.util.Comparator;

public class UsernameComparator implements Comparator<User> {
    @Override
    public int compare(User o1, User o2) {
        if(o1 == null) {
            return -1;
        }
        if(o2 == null) {
            return 1;
        }
        return o1.getLoginId().compareTo(o2.getLoginId());
    }
}
