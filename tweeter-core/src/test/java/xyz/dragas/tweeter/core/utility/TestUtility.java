package xyz.dragas.tweeter.core.utility;

import xyz.dragas.tweeter.api.model.Tag;
import xyz.dragas.tweeter.api.model.Tweet;
import xyz.dragas.tweeter.api.model.User;

public class TestUtility {
    public static final String PASSWORD = "PASSWORD";
    public static final String LOGIN = "LOGIN";
    public static final String EMAIL = "EMAIL";
    public static final String LAST = "LAST";
    public static final String NAME = "NAME";
    public static final String CONTACT = "CONTACT";
    public static final String MESSAGE = "MESSAGE";

    public static User createUser() {
        User u = new User();
        u.setContactNumber(CONTACT);
        u.setFirstName(NAME);
        u.setLastName(LAST);
        u.setEmail(EMAIL);
        u.setLoginId(LOGIN);
        u.setPassword(PASSWORD);
        return u;
    }

    public static Tweet createTweet() {
        return createTweet(null, createUser());
    }

    public static Tweet createTweet(User author) {
        return createTweet(null, author);
    }

    public static Tweet createTweet(Tweet parent, User author) {
        Tweet tweet = new Tweet();
        if(parent != null)
            tweet.setParent(parent.getId());
        tweet.setAuthor(author);
        tweet.setMessage(MESSAGE);
        tweet.setId("");
        return tweet;
    }

    public static Tag createTag() {
        Tag tag = new Tag();
        tag.setName(NAME);
        return tag;
    }
}
