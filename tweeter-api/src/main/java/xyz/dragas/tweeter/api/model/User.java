package xyz.dragas.tweeter.api.model;

import java.util.Collection;
import java.util.TreeSet;

public class User implements Comparable<User> {
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String loginId;
    private String contactNumber;
    private String id;
    private Collection<Tweet> likedTweets = new TreeSet<>();

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public int compareTo(User o) {
        if (o == null) {
            return 1;
        }
        return CharSequence.compare(getId(), o.getId());
    }

    public Collection<Tweet> getLikedTweets() {
        return likedTweets;
    }

    public void setLikedTweets(Collection<Tweet> likedTweets) {
        this.likedTweets = likedTweets;
    }
}
