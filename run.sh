#!/usr/bin/env bash

java -Dlogback.configurationFile=configuration/logback-logstash.xml \
 -DLOGSTASH_URL=logstash.infra:4560 \
 -DKAFKA_URL=kafka.infra:9092 \
 "-Dspring.data.mongodb.uri=$MONGODB_URL" \
 -XX:MaxHeapSize=512m \
 -XX:ThreadStackSize=1024k \
 --class-path "lib/*" \
 xyz.dragas.tweeter.spring.Main \
 --spring.config.location=configuration/application.properties
