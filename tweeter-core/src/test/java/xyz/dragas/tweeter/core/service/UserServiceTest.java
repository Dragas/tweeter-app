package xyz.dragas.tweeter.core.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import xyz.dragas.tweeter.api.model.User;
import xyz.dragas.tweeter.api.repository.UserRepository;
import xyz.dragas.tweeter.api.service.UserService;
import xyz.dragas.tweeter.core.repository.InMemoryUserRepository;
import xyz.dragas.tweeter.core.utility.TestUtility;


public class UserServiceTest {

    private UserService userService;
    private UserRepository userRepository;

    @BeforeEach
    public void setUp() {
        userRepository = new InMemoryUserRepository();
        userService = new DefaultUserService(userRepository);
    }

    @Test
    public void serviceStoresUser() {
        User nonsavedUser = TestUtility.createUser();
        User user = userService.createUser(nonsavedUser);
        Assertions.assertNotEquals(null, user.getId(), "Id must not be null");
        Assertions.assertNull(nonsavedUser.getId(), "Id must not be modified");
        User repositoryUser = userRepository.get(user.getId());
        Assertions.assertNotNull(user, "User must be stored in repository");
        Assertions.assertEquals(repositoryUser, user, "User object must be the same from repository");
    }

    @Test
    public void serviceRetainsFieldValues() {
        User u = TestUtility.createUser();
        User createdUser = userService.createUser(u);
        Assertions.assertEquals(createdUser.getFirstName(), u.getFirstName(), "Values must match those provided in source");
        Assertions.assertEquals(createdUser.getLastName(), u.getLastName(), "Values must match those provided in source");
        Assertions.assertEquals(createdUser.getEmail(), u.getEmail(), "Values must match those provided in source");
        Assertions.assertEquals(createdUser.getPassword(), u.getPassword(), "Values must match those provided in source");
        Assertions.assertEquals(createdUser.getLoginId(), u.getLoginId(), "Values must match those provided in source");
        Assertions.assertEquals(createdUser.getContactNumber(), u.getContactNumber(), "Values must match those provided in source");
    }

    @Test
    public void serviceGetsExistingUserId() {
        User u = TestUtility.createUser();
        u = userRepository.store(u);
        String id = u.getId();
        User existingUser = userService.getUser(id);
        Assertions.assertEquals(u, existingUser, "User must be obtained from internal repo");
    }

    @Test
    public void serviceGetsExistingUserByName() {
        User u = TestUtility.createUser();
        u = userRepository.store(u);
        String name = u.getLoginId();
        User existingUser = userService.getUserByUsername(name);
        Assertions.assertEquals(u, existingUser, "User must be obtained from internal repo");
    }

}
