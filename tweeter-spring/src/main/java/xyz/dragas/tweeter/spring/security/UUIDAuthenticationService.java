package xyz.dragas.tweeter.spring.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import xyz.dragas.tweeter.api.model.User;
import xyz.dragas.tweeter.api.service.UserService;

import java.util.*;

@Service
public class UUIDAuthenticationService implements UserAuthenticationService {

    private static final GrantedAuthority USER = new GrantedAuthority() {
        @Override
        public String getAuthority() {
            return "USER";
        }
    };
    private final UserService userService;

    private final AuthenticationRepository authrepository;

    public UUIDAuthenticationService(UserService userService, AuthenticationRepository repository) {
        this.userService = userService;
        this.authrepository = repository;
    }

    @Override
    public Optional<String> login(String username, String password) {
        User user = userService.getUserByUsername(username);
        if(user == null) {
            return Optional.empty();
        }
        if(!user.getPassword().equals(hash(password))) {
            return Optional.empty();
        }
        UserDetails details = authrepository.getUserDetails(username);
        String uuid = authrepository.getTokenForUser(details.getUsername());
        return Optional.of(uuid);
    }

    private String hash(String password) {
        return password;
    }

    @Override
    public Optional<UserDetails> findByToken(String token) {
        return Optional.ofNullable(authrepository.getUserByToken(token));
    }

    @Override
    public void logout(UserDetails user) {
        authrepository.logout(user);
    }
}
