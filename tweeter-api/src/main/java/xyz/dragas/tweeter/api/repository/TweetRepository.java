package xyz.dragas.tweeter.api.repository;

import xyz.dragas.tweeter.api.model.Tweet;
import xyz.dragas.tweeter.api.model.User;

import java.util.Collection;

public interface TweetRepository {

    Tweet store(Tweet tweet);

    void delete(Tweet tweet);

    Tweet get(String id);

    Tweet update(Tweet tweet);

    Collection<Tweet> getTweets(User user);

    Collection<Tweet> getTweets();
}
