FROM openjdk:11.0.10-jdk

COPY "." "/app"

WORKDIR "/app"

ENTRYPOINT "/app/run.sh"