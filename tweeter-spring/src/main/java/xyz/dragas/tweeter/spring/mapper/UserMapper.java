package xyz.dragas.tweeter.spring.mapper;

import xyz.dragas.tweeter.api.model.Tweet;
import xyz.dragas.tweeter.api.model.User;
import xyz.dragas.tweeter.spring.model.RegisterUserRequest;
import xyz.dragas.tweeter.spring.model.UserResponse;

import java.util.stream.Collectors;

public class UserMapper {

    public User toUser(RegisterUserRequest registerUserRequest) {
        User u = new User();
        u.setFirstName(registerUserRequest.getFirstName());
        u.setLastName(registerUserRequest.getLastName());
        u.setEmail(registerUserRequest.getEmail());
        u.setPassword(registerUserRequest.getPassword());
        u.setLoginId(registerUserRequest.getLoginId());
        u.setContactNumber(registerUserRequest.getContactNumber());
        return u;
    }

    public UserResponse toResponse(User user) {
        UserResponse response = new UserResponse();
        response.setUsername(user.getLoginId());
        response.getLikedTweets().addAll(user.getLikedTweets().stream().map(Tweet::getId).collect(Collectors.toUnmodifiableSet()));
        return response;
    }
}
