package xyz.dragas.tweeter.core.utility;

import xyz.dragas.tweeter.api.model.User;

import java.util.function.Predicate;

public class UsernameContainsPredicate implements Predicate<User> {
    private final String username;

    public UsernameContainsPredicate(String username) {
        this.username = username;
    }

    @Override
    public boolean test(User user) {
        return user.getLoginId().contains(username);
    }
}
