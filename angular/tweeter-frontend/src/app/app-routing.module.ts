import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from "./login/login.component";
import {LoginGuardGuard} from "./login-guard.guard";
import {MainComponent} from "./main/main.component";
import {NotLoginGuard} from "./not-login.guard";
import {NotFoundComponent} from "./not-found/not-found.component";
import {RegisterComponent} from "./register/register.component";
import {LogoutComponent} from "./logout/logout.component";

const routes: Routes = [
  {
    path: "login", canActivate: [NotLoginGuard], component: LoginComponent
  },
  {
    path: "", pathMatch: "full", component: MainComponent, canActivate: [LoginGuardGuard]
  },
  {
    path: "register", pathMatch: "full", component: RegisterComponent, canActivate: [NotLoginGuard]
  },
  {
    path: "logout", pathMatch: "full", component: LogoutComponent, canActivate: [LoginGuardGuard]
  },
  {
    path: "**", component: NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
