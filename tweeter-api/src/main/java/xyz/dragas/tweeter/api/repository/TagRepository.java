package xyz.dragas.tweeter.api.repository;

import xyz.dragas.tweeter.api.model.Tag;

import java.util.Collection;

public interface TagRepository {

    Tag store(Tag tag);

    void delete(Tag tag);

    Tag get(String id);

    Collection<Tag> getByNamePart(String name);
}
