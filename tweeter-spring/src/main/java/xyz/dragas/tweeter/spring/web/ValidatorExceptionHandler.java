package xyz.dragas.tweeter.spring.web;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolation;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@RestControllerAdvice
public class ValidatorExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

        Map<String, List<String>> messages = ex.getAllErrors()
                .stream()
                .map(it -> it.unwrap(ConstraintViolation.class))
                .collect(
                        Collectors.groupingBy(
                                (it) -> it.getPropertyPath().toString(),
                                Collectors.mapping(ConstraintViolation::getMessageTemplate, Collectors.toList())
                        )
                );
        return new ResponseEntity<>(messages, HttpStatus.BAD_REQUEST);
    }
}
