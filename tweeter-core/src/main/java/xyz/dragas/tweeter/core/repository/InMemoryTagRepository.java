package xyz.dragas.tweeter.core.repository;

import xyz.dragas.tweeter.api.model.Tag;
import xyz.dragas.tweeter.api.repository.TagRepository;
import xyz.dragas.tweeter.core.utility.TagNameContainsPredicate;

import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

public class InMemoryTagRepository implements TagRepository {
    private final Map<String, Tag> tags = Collections.synchronizedMap(new TreeMap<>());
    private final AtomicLong idGen = new AtomicLong(1);

    @Override
    public Tag store(Tag tag) {
        Tag cloned = new Tag();
        cloned.setName(tag.getName());
        cloned.setId(UUID.randomUUID().toString());
        tags.put(cloned.getId(), cloned);
        return cloned;
    }

    @Override
    public void delete(Tag tag) {
        String id = tag.getId();
        tags.remove(id);
    }

    @Override
    public Tag get(String id) {
        return tags.get(id);
    }

    @Override
    public Collection<Tag> getByNamePart(String name) {
        return tags
                .values()
                .stream()
                .filter(new TagNameContainsPredicate(name))
                .collect(Collectors.toUnmodifiableSet());
    }
}
