package xyz.dragas.tweeter.spring.service;

import xyz.dragas.tweeter.spring.model.LoginUserRequest;
import xyz.dragas.tweeter.spring.model.RegisterUserRequest;
import xyz.dragas.tweeter.spring.model.UserResponse;

import java.util.Collection;

public interface UserManagementService {
    UserResponse registerUser(RegisterUserRequest registerUserRequest);
    String loginUser(LoginUserRequest loginUserRequest);
    Collection<UserResponse> getUsers();
    Collection<UserResponse> getUsers(String namePart);
}
