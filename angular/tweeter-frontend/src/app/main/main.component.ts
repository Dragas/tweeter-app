import { Component, OnInit } from '@angular/core';
import {TweetResponse} from "../api/models/tweet-response";
import {TweetControllerService} from "../api/services/tweet-controller.service";
import {UserService} from "../user.service";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  tweets: Array<TweetResponse>;
  private api: TweetControllerService;
  private user: UserService;

  constructor(api: TweetControllerService, user: UserService) {
    this.api = api;
    this.user = user;
    this.tweets = [];
  }

  ngOnInit(): void {
    this.api
      .getAllTweetsUsingGet({
        "X-Authorization": this.user.getToken()
      }).subscribe((it) => this.tweets = it);
  }

  onTweetCreate(tweet : TweetResponse) {
    this.tweets.push(tweet);
  }
}
