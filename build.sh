#!/usr/bin/env bash
set -x
rm -rf app
./mvnw clean package
./mvnw dependency:copy-dependencies --projects :tweeter-spring
./mvnw assembly:single --projects :tweeter-spring
mkdir app
mv tweeter-spring/target/*.zip app/
cd app
unzip *.zip
mv */** .