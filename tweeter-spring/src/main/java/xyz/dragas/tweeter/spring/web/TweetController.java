package xyz.dragas.tweeter.spring.web;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import xyz.dragas.tweeter.spring.model.*;
import xyz.dragas.tweeter.spring.service.TweetManagementService;
import xyz.dragas.tweeter.spring.service.UserManagementService;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/api/v1.0/tweets", produces = MediaType.APPLICATION_JSON_VALUE)
public class TweetController {
    private final UserManagementService userService;
    private final TweetManagementService tweetService;

    public TweetController(
            UserManagementService userService,
            TweetManagementService tweetService
    ) {
        this.userService = userService;
        this.tweetService = tweetService;
    }

    @RequestMapping(method = RequestMethod.POST, path = "/register")
    public UserResponse registerUser(@Valid @RequestBody RegisterUserRequest registerUserRequest) {
        return userService.registerUser(registerUserRequest);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/login", consumes = MediaType.APPLICATION_JSON_VALUE)
    public String login(@Valid @RequestBody LoginUserRequest loginUserRequest) {
        return String.format("\"Bearer %s\"", userService.loginUser(loginUserRequest));
    }

    @RequestMapping(method = RequestMethod.GET, path = "/all")
    @ApiImplicitParam(name = "X-" + HttpHeaders.AUTHORIZATION, paramType = "header", required = true, allowEmptyValue = false)
    public Collection<TweetResponse> getAllTweets(@AuthenticationPrincipal UserDetails details) {
        return tweetService.getTweets();
    }

    @RequestMapping(method = RequestMethod.GET, path = "/users/all")
    @ApiImplicitParam(name = "X-" + HttpHeaders.AUTHORIZATION, paramType = "header", required = true, allowEmptyValue = false)
    public Collection<UserResponse> getUsers(@AuthenticationPrincipal UserDetails details) {
        return userService.getUsers();
    }

    @RequestMapping(method = RequestMethod.GET, path = "/user/search/{username}")
    @ApiImplicitParam(name = "X-" + HttpHeaders.AUTHORIZATION, paramType = "header", required = true, allowEmptyValue = false)
    public Collection<UserResponse> getUsers(@AuthenticationPrincipal UserDetails details, @PathVariable("username") String username) {
        return userService.getUsers(username);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/{username}")
    @ApiImplicitParam(name = "X-" + HttpHeaders.AUTHORIZATION, paramType = "header", required = true, allowEmptyValue = false)
    public Collection<TweetResponse> getTweets(@AuthenticationPrincipal UserDetails details, @PathVariable("username") String username) {
        return tweetService.getTweets(username);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/{username}/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "username", paramType = "path", required = false),
            @ApiImplicitParam(name = "X-" + HttpHeaders.AUTHORIZATION, paramType = "header", required = true, allowEmptyValue = false)
    })
    public TweetResponse createTweet(@AuthenticationPrincipal UserDetails details, @Valid @RequestBody TweetRequest request) {
        return tweetService.createTweet(details, request);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/{username}/update/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "username", paramType = "path", required = false),
            @ApiImplicitParam(name = "X-" + HttpHeaders.AUTHORIZATION, paramType = "header", required = true, allowEmptyValue = false)
    })
    public TweetResponse updateTweet(@AuthenticationPrincipal UserDetails details, @Min(value = 1) @PathVariable("id") String id, @RequestBody @NotBlank @NotNull String message) {
        return tweetService.updateTweet(details, id, message);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/{username}/delete/{id}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "username", paramType = "path", required = false),
            @ApiImplicitParam(name = "X-" + HttpHeaders.AUTHORIZATION, paramType = "header", required = true, allowEmptyValue = false)
    })
    public TweetResponse deleteTweet(@AuthenticationPrincipal UserDetails details, @Min(value = 1) @PathVariable("id") String id) {
        return tweetService.deleteTweet(details, id);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/{username}/like/{id}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "username", paramType = "path", required = false),
            @ApiImplicitParam(name = "X-" + HttpHeaders.AUTHORIZATION, paramType = "header", required = true, allowEmptyValue = false)
    })
    public TweetResponse likeTweet(@AuthenticationPrincipal UserDetails details, @Min(value = 1) @PathVariable("id") String id) {
        return tweetService.likeTweet(details, id);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/{username}/reply/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "username", paramType = "path", required = false),
            @ApiImplicitParam(name = "X-" + HttpHeaders.AUTHORIZATION, paramType = "header", required = true, allowEmptyValue = false)
    })
    public TweetResponse replyTweet(@AuthenticationPrincipal UserDetails details, @Min(value = 1) @PathVariable("id") String id, @Valid @RequestBody TweetRequest request) {
        return tweetService.replyTweet(details, id, request);
    }
}
