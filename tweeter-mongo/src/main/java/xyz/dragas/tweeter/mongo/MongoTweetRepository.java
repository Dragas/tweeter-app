package xyz.dragas.tweeter.mongo;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import xyz.dragas.tweeter.api.model.Tweet;
import xyz.dragas.tweeter.api.model.User;
import xyz.dragas.tweeter.api.repository.TweetRepository;

import java.util.Collection;

public class MongoTweetRepository implements TweetRepository {

    private final MongoTemplate mongoClient;

    public MongoTweetRepository(MongoTemplate mongoTemplate) {
        this.mongoClient = mongoTemplate;
    }

    @Override
    public Tweet store(Tweet tweet) {
        return mongoClient.insert(tweet, "tweets");
    }

    @Override
    public void delete(Tweet tweet) {
        mongoClient.findAllAndRemove(
                Query.query(Criteria.where("_id").is(tweet.getId())), "tweets"
        );
    }

    @Override
    public Tweet get(String id) {
        return mongoClient.findOne(
                Query.query(Criteria.where("_id").is(id)), Tweet.class, "tweets"
        );
    }

    @Override
    public Tweet update(Tweet tweet) {
        return mongoClient.findAndReplace(Query.query(Criteria.where("id").is(tweet.getId())),tweet, "tweets");
    }

    @Override
    public Collection<Tweet> getTweets(User user) {
        return mongoClient.find(Query.query(Criteria.where("author.id").is(user.getId())), Tweet.class, "tweets");
    }

    @Override
    public Collection<Tweet> getTweets() {
        return mongoClient.findAll(Tweet.class, "tweets");
    }
}
