export interface TweetErrorResponse {
  message?: Array<string>,
  tag?: Array<string>
}
