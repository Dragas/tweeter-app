import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {UserService} from "../user.service";

@Component({
  selector: 'app-mainbar',
  templateUrl: './mainbar.component.html',
  styleUrls: ['./mainbar.component.scss']
})
export class MainbarComponent implements OnInit {
  private route: Router;
  private userService: UserService;

  constructor(router: Router, userService: UserService) {
    this.route = router;
    this.userService = userService;
  }

  ngOnInit(): void {

  }

  isLoginRoute(): boolean {
    return this.isCurrentRoute("/login");
  }

  isLoggedIn(): boolean {
    return this.userService.isLoggedIn();
  }

  isCurrentRoute(route: string) {
    return this.route.url === route;
  }
}
