import {Component, OnInit} from '@angular/core';
import {TweetControllerService} from "../api/services/tweet-controller.service";
import {UserService} from "../user.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  username: string;
  password: string;
  private api: TweetControllerService;
  error: boolean;
  loading: boolean;
  private userService: UserService;
  private router: Router;

  constructor(api: TweetControllerService, userService: UserService, router: Router) {
    this.username = "";
    this.password = "";
    this.error = false;
    this.loading = false;
    this.api = api;
    this.userService = userService;
    this.router = router;
  }

  ngOnInit(): void {
  }

  onSubmit($event: Event) {
    this.error = false;
    this.loading = true;
    this.api.loginUsingPost({
      body: {
        password: this.password,
        username: this.username
      }
    }).subscribe((it) => {
        if(it.endsWith(" ")) {
          this.setError(null);
          return;
        }
        this.userService.setToken(it);
        this.router.navigate(["/"])
      },
      (error) => {
        this.setError(error);
      });
  }

  private setError(error: any) {
    this.error = true;
    this.loading = false;
  }
}
