package xyz.dragas.tweeter.spring.validation.password;

import xyz.dragas.tweeter.spring.model.RegisterUserRequest;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class MatchingPasswordValidator implements ConstraintValidator<MatchingPassword, RegisterUserRequest> {
    @Override
    public void initialize(MatchingPassword constraintAnnotation) {

    }

    @Override
    public boolean isValid(RegisterUserRequest value, ConstraintValidatorContext context) {
        if(value == null) {
            return true;
        }
        String password = value.getPassword();
        String confirmation = value.getPasswordConfirmation();
        if(password != null && confirmation != null) {
            return password.equals(confirmation);
        }
        return false;
    }
}
