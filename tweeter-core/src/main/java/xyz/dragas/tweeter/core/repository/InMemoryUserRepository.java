package xyz.dragas.tweeter.core.repository;

import xyz.dragas.tweeter.api.model.User;
import xyz.dragas.tweeter.api.repository.UserRepository;
import xyz.dragas.tweeter.core.utility.UsernameComparator;
import xyz.dragas.tweeter.core.utility.UsernameContainsPredicate;
import xyz.dragas.tweeter.core.utility.UsernamePredicate;

import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

public class InMemoryUserRepository implements UserRepository {
    private final Map<String, User> users = Collections.synchronizedMap(new TreeMap<>());
    private final AtomicLong idGen = new AtomicLong(1);
    @Override
    public User store(User user) {
        User cloned = new User();
        cloned.setFirstName(user.getFirstName());
        cloned.setLastName(user.getLastName());
        cloned.setEmail(user.getEmail());
        cloned.setPassword(user.getPassword());
        cloned.setLoginId(user.getLoginId());
        cloned.setContactNumber(user.getContactNumber());
        cloned.setId(UUID.randomUUID().toString());
        users.put(cloned.getId(), cloned);
        return cloned;
    }

    @Override
    public void delete(User user) {
        String id = user.getId();
        users.remove(id);
    }

    @Override
    public User get(String id) {
        return users.get(id);
    }

    @Override
    public User getByUsername(String username) {
        return users
                .values()
                .stream()
                .sorted(new UsernameComparator())
                .filter(new UsernamePredicate(username))
                .findFirst()
                .orElse(null);
    }

    @Override
    public Collection<User> getAllUsers() {
        return users.values();
    }

    @Override
    public Collection<User> getUsersByFragment(String username) {
        return users
                .values()
                .stream()
                .filter(new UsernameContainsPredicate(username))
                .collect(Collectors.toUnmodifiableSet());
    }
}
