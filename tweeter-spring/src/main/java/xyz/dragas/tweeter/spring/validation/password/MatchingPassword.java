package xyz.dragas.tweeter.spring.validation.password;

import javax.validation.Constraint;
import javax.validation.Payload;

@Constraint(validatedBy = {})
public @interface MatchingPassword {
    String message() default "{xyz.dragas.tweeter.spring.validation.password}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
