package xyz.dragas.tweeter.core.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import xyz.dragas.tweeter.api.model.Tag;
import xyz.dragas.tweeter.api.model.Tweet;
import xyz.dragas.tweeter.api.model.User;
import xyz.dragas.tweeter.api.repository.TweetRepository;
import xyz.dragas.tweeter.api.service.TweetService;
import xyz.dragas.tweeter.core.repository.InMemoryTweetRepository;
import xyz.dragas.tweeter.core.utility.TestUtility;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.Collection;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class TweetServiceTest {

    private Clock clock;
    private ZoneId zone;
    private Instant now;
    private TweetRepository tweetRepository;
    private TweetService tweetService;

    @BeforeEach
    public void setUp() {
        zone = ZoneId.systemDefault();
        now = Instant.now();
        clock = Clock.fixed(now, zone);
        tweetRepository = new InMemoryTweetRepository(clock);
        tweetService = new DefaultTweetService(tweetRepository);
    }

    @Test
    public void serviceStoresTweet() {
        Tweet tweet = TestUtility.createTweet();
        Tweet savedTweet = tweetService.createTweet(tweet);
        Assertions.assertNotEquals(tweet, savedTweet, "Must be two different objects");
        Assertions.assertNotEquals(0, savedTweet.getId(), "Must have assigned id");
        Tweet repositoryTweet = tweetRepository.get(savedTweet.getId());
        Assertions.assertNotNull(repositoryTweet, "must be stored in repository");
        Assertions.assertEquals(repositoryTweet, savedTweet, "two objects must be the same");
        Assertions.assertNotNull(savedTweet.getCreatedAt(), "Must have created at value");
        Assertions.assertEquals(ZonedDateTime.now(clock), savedTweet.getCreatedAt(), "date must be from same clock");
    }

    @Test
    public void serviceRetainsTweetValues() {
        Tweet tweet = TestUtility.createTweet();
        Tweet savedTweet = tweetService.createTweet(tweet);
        Assertions.assertEquals(tweet.getMessage(), savedTweet.getMessage(), "field value must be retained");
        Assertions.assertEquals(tweet.getAuthor(), savedTweet.getAuthor(), "field value must be retained");
        Assertions.assertEquals(tweet.getParent(), savedTweet.getParent(), "field value must be retained");
    }

    @Test
    public void serviceSetsTag() {
        Tag tag = TestUtility.createTag();
        Tweet tweet = TestUtility.createTweet();
        tweetService.associateTag(tweet, tag);
        Assertions.assertNotNull(tweet.getTag(), "Tag must not be null");
        Assertions.assertEquals(tag, tweet.getTag(), "tag must be equal");
    }

    @Test
    public void serviceAddsResponse() {
        Tweet parent = TestUtility.createTweet();
        Tweet reply = TestUtility.createTweet();
        tweetService.replyToTweet(parent, reply);
        Assertions.assertEquals(parent.getId(), reply.getParent(), "parent must be in reply");
        Assertions.assertTrue(parent.getResponses().contains(reply), "reply must be in responses");
    }

    @Test
    public void serviceGetsTweetById() {
        Tweet tweet = TestUtility.createTweet();
        tweet = tweetRepository.store(tweet);
        String id = tweet.getId();
        Tweet savedTweet = tweetService.getTweet(id);
        Assertions.assertEquals(tweet, savedTweet, "two objects must match");
    }

    @Test
    public void serviceGetsUsersTweets() {
        User user = TestUtility.createUser();
        Collection<Tweet> tweets = Arrays.asList(TestUtility.createTweet(user), TestUtility.createTweet(user));
        tweets = tweets.stream().map(tweetRepository::store).collect(Collectors.toCollection(TreeSet::new));
        Collection<Tweet> savedTweets = tweetService.getTweets(user);
        Assertions.assertNotEquals(0, savedTweets.size(), "Must contain atleast 1");
        Assertions.assertEquals(tweets.size(), savedTweets.size(), "Must be same size");
        for (Tweet tweet : tweets) {
            Assertions.assertTrue(savedTweets.contains(tweet), "Must contain particular tweet");
        }
    }
}
