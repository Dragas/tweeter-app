package xyz.dragas.tweeter.core.utility;

import xyz.dragas.tweeter.api.model.Tag;

import java.util.function.Predicate;

public class TagNameContainsPredicate implements Predicate<Tag> {
    private final String name;

    public TagNameContainsPredicate(String name) {
        this.name = name;
    }

    @Override
    public boolean test(Tag tag) {
        return tag.getName().contains(name);
    }
}
