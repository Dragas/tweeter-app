package xyz.dragas.tweeter.spring.model;

public class TweetRequest {
    private String message;
    private String tag;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
