package xyz.dragas.tweeter.spring.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.*;


public class InMemoryAuthenticationRepository implements AuthenticationRepository {
    private static final GrantedAuthority USER = new GrantedAuthority() {
        @Override
        public String getAuthority() {
            return "USER";
        }
    };
    private final Map<String, UserDetails> authMap = Collections.synchronizedMap(new TreeMap<>());

    @Override
    public UserDetails getUserDetails(String username) {
        String uuid = getTokenForUser(username);
        UserDetails details = getUserByToken(uuid);
        if(details == null) {
            details = new UserDetails() {
                @Override
                public Collection<? extends GrantedAuthority> getAuthorities() {
                    return Collections.singleton(USER);
                }

                @Override
                public String getPassword() {
                    return "";
                }

                @Override
                public String getUsername() {
                    return username;
                }

                @Override
                public boolean isAccountNonExpired() {
                    return true;
                }

                @Override
                public boolean isAccountNonLocked() {
                    return true;
                }

                @Override
                public boolean isCredentialsNonExpired() {
                    return true;
                }

                @Override
                public boolean isEnabled() {
                    return true;
                }
            };
        }
        else {
            authMap.remove(uuid);
        }
        uuid = UUID.randomUUID().toString();
        authMap.put(uuid, details);
        return details;
    }

    @Override
    public String getTokenForUser(String username) {
        String uuid = null;
        for(Map.Entry<String, UserDetails> entry : authMap.entrySet()) {
            if(entry.getValue().getUsername().equals(username)) {
                uuid = entry.getKey();
                break;
            }
        }
        return uuid;
    }

    @Override
    public UserDetails getUserByToken(String uuid) {
        return authMap.get(uuid);
    }

    @Override
    public void logout(UserDetails user) {
        String token = null;
        for(Map.Entry<String, UserDetails> entry : authMap.entrySet()) {
            UserDetails details = entry.getValue();
            if(user.getUsername().equals(details.getUsername())) {
                token = entry.getKey();
                break;
            }
        }
        authMap.remove(token);
    }
}
