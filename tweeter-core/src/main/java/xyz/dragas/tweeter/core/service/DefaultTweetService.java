package xyz.dragas.tweeter.core.service;

import xyz.dragas.tweeter.api.model.Tag;
import xyz.dragas.tweeter.api.model.Tweet;
import xyz.dragas.tweeter.api.model.User;
import xyz.dragas.tweeter.api.repository.TweetRepository;
import xyz.dragas.tweeter.api.service.TweetService;

import java.util.Collection;

public class DefaultTweetService implements TweetService {

    private final TweetRepository tweetRepository;

    public DefaultTweetService(TweetRepository tweetRepository) {
        this.tweetRepository = tweetRepository;
    }

    @Override
    public Tweet createTweet(Tweet tweet) {
        return tweetRepository.store(tweet);
    }

    @Override
    public Tweet replyToTweet(Tweet parent, Tweet response) {
        parent.getResponses().add(response);
        response.setParent(parent.getId());
        tweetRepository.update(parent);
        response = tweetRepository.update(response);
        return response;
    }

    @Override
    public Tweet associateTag(Tweet tweet, Tag tag) {
        tweet.setTag(tag);
        tweet = tweetRepository.update(tweet);
        return tweet;
    }

    @Override
    public Collection<Tweet> getTweets(User user) {
        return tweetRepository.getTweets(user);
    }

    @Override
    public Collection<Tweet> getTweets() {
        return tweetRepository.getTweets();
    }

    @Override
    public Tweet getTweet(String id) {
        return tweetRepository.get(id);
    }

    @Override
    public void deleteTweet(Tweet tweet) {
        tweetRepository.delete(tweet);
    }

    @Override
    public void updateContent(Tweet tweet, String content) {
        tweet.setMessage(content);
        tweetRepository.update(tweet);
    }
}
