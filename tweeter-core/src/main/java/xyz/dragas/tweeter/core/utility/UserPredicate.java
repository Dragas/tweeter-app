package xyz.dragas.tweeter.core.utility;

import xyz.dragas.tweeter.api.model.Tweet;
import xyz.dragas.tweeter.api.model.User;

import java.util.function.Predicate;

public class UserPredicate implements Predicate<Tweet> {
    private final User user;

    public UserPredicate(User user) {
        this.user = user;
    }

    @Override
    public boolean test(Tweet tweet) {
        return tweet.getAuthor().getId() == user.getId();
    }
}
