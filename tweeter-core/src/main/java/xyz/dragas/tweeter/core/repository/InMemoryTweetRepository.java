package xyz.dragas.tweeter.core.repository;

import xyz.dragas.tweeter.api.model.Tweet;
import xyz.dragas.tweeter.api.model.User;
import xyz.dragas.tweeter.api.repository.TweetRepository;
import xyz.dragas.tweeter.core.utility.UserPredicate;

import java.time.Clock;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

public class InMemoryTweetRepository implements TweetRepository {
    private final Map<String, Tweet> tweets = Collections.synchronizedMap(new TreeMap<>());
    private final AtomicLong idGen = new AtomicLong(1);
    private final Clock clock;

    public InMemoryTweetRepository(Clock clock) {
        this.clock = clock;
    }

    @Override
    public Tweet store(Tweet tweet) {
        Tweet cloned = new Tweet();
        cloned.setAuthor(tweet.getAuthor());
        cloned.setMessage(tweet.getMessage());
        cloned.setParent(tweet.getParent());
        cloned.setTag(tweet.getTag());
        cloned.setId(UUID.randomUUID().toString());
        cloned.setCreatedAt(ZonedDateTime.now(clock));
        tweets.put(cloned.getId(), cloned);
        return cloned;
    }

    @Override
    public void delete(Tweet tweet) {
        String id = tweet.getId();
        tweets.remove(id);
    }

    @Override
    public Tweet get(String id) {
        return tweets.get(id);
    }

    @Override
    public Tweet update(Tweet tweet) {
        return tweet;
    }

    @Override
    public Collection<Tweet> getTweets(User user) {
        return tweets
                .values()
                .stream()
                .filter(new UserPredicate(user))
                .collect(Collectors.toUnmodifiableSet());
    }

    @Override
    public Collection<Tweet> getTweets() {
        return tweets.values();
    }
}
