package xyz.dragas.tweeter.api.service;

import xyz.dragas.tweeter.api.model.Tag;
import xyz.dragas.tweeter.api.model.Tweet;
import xyz.dragas.tweeter.api.model.User;

import java.util.Collection;

public interface TweetService {

    Tweet createTweet(Tweet tweet);

    Tweet replyToTweet(Tweet parent, Tweet response);

    Tweet associateTag(Tweet tweet, Tag tag);

    Collection<Tweet> getTweets(User user);

    Collection<Tweet> getTweets();

    Tweet getTweet(String id);

    void deleteTweet(Tweet tweet);

    void updateContent(Tweet tweet, String content);
}
