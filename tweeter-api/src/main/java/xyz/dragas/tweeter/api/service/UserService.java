package xyz.dragas.tweeter.api.service;

import xyz.dragas.tweeter.api.model.Tweet;
import xyz.dragas.tweeter.api.model.User;

import java.util.Collection;

public interface UserService {

    User createUser(User user);

    User getUser(String id);

    User getUserByUsername(String username);

    Collection<User> getUsers();

    Collection<User> getUsers(String username);

    void likeTweet(User user, Tweet tweet);
}
