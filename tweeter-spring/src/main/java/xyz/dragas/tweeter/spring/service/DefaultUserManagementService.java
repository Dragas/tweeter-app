package xyz.dragas.tweeter.spring.service;

import org.springframework.stereotype.Service;
import xyz.dragas.tweeter.api.model.User;
import xyz.dragas.tweeter.api.service.UserService;
import xyz.dragas.tweeter.spring.mapper.UserMapper;
import xyz.dragas.tweeter.spring.model.LoginUserRequest;
import xyz.dragas.tweeter.spring.model.RegisterUserRequest;
import xyz.dragas.tweeter.spring.model.UserResponse;
import xyz.dragas.tweeter.spring.security.UserAuthenticationService;

import java.util.Collection;
import java.util.stream.Collectors;

@Service
public class DefaultUserManagementService implements UserManagementService{

    private final UserService userService;
    private final UserMapper mapper;
    private final UserAuthenticationService userAuthenticationService;

    public DefaultUserManagementService(UserService userService, UserMapper mapper, UserAuthenticationService userAuthenticationService) {
        this.userService = userService;
        this.mapper = mapper;
        this.userAuthenticationService = userAuthenticationService;
    }

    @Override
    public UserResponse registerUser(RegisterUserRequest registerUserRequest) {
        User user = mapper.toUser(registerUserRequest);
        user = userService.createUser(user);
        return mapper.toResponse(user);
    }

    @Override
    public String loginUser(LoginUserRequest loginUserRequest) {
        return userAuthenticationService.login(loginUserRequest.getUsername(), loginUserRequest.getPassword()).orElse("");
    }

    @Override
    public Collection<UserResponse> getUsers() {
        return userService.getUsers().stream().map(mapper::toResponse).collect(Collectors.toUnmodifiableList());
    }

    @Override
    public Collection<UserResponse> getUsers(String namePart) {
        return userService.getUsers(namePart).stream().map(mapper::toResponse).collect(Collectors.toUnmodifiableSet());
    }
}
