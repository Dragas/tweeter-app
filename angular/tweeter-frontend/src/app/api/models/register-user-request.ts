/* tslint:disable */
/* eslint-disable */
export interface RegisterUserRequest {
  contactNumber?: string;
  email?: string;
  firstName?: string;
  lastName?: string;
  loginId?: string;
  password?: string;
  passwordConfirmation?: string;
}
