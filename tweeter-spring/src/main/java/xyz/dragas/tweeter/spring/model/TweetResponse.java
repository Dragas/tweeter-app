package xyz.dragas.tweeter.spring.model;

import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.Comparator;

public class TweetResponse implements Comparable<TweetResponse> {

    private ZonedDateTime createdAt;
    private String author;
    private String message;
    private Collection<TweetResponse> responses;
    private String id;
    private String tag;

    @Override
    public int compareTo(TweetResponse o) {
        return CharSequence.compare(getId(), o.getId());
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getAuthor() {
        return author;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setResponses(Collection<TweetResponse> responses) {
        this.responses = responses;
    }

    public Collection<TweetResponse> getResponses() {
        return responses;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getTag() {
        return tag;
    }
}
