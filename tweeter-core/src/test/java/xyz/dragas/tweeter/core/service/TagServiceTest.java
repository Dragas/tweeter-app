package xyz.dragas.tweeter.core.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import xyz.dragas.tweeter.api.model.Tag;
import xyz.dragas.tweeter.api.repository.TagRepository;
import xyz.dragas.tweeter.api.service.TagService;
import xyz.dragas.tweeter.core.repository.InMemoryTagRepository;
import xyz.dragas.tweeter.core.utility.TestUtility;

import java.util.Collection;

public class TagServiceTest {
    private TagRepository tagRepository;
    private TagService tagService;


    @BeforeEach
    public void setUp() {
        tagRepository = new InMemoryTagRepository();
        tagService = new DefaultTagService(tagRepository);
    }

    @Test
    public void serviceStoresTag() {
        Tag tag = TestUtility.createTag();
        Tag saved = tagService.createTag(tag);
        Assertions.assertNotNull(saved, "saved tag must not be null");
        Assertions.assertNotEquals(0, saved.getId(), "Id must be present");
        Tag repositoryTag = tagRepository.get(saved.getId());
        Assertions.assertEquals(repositoryTag, saved, "must be same from repo");
    }

    @Test
    public void serviceGetsTagsByNamePart() {
        Tag tag = TestUtility.createTag();
        tagRepository.store(tag);
        tag.setName(tag.getName() + tag.getName());
        tagRepository.store(tag);
        tag.setName("M<");
        tagRepository.store(tag);
        Collection<Tag> tags = tagService.getTagsByNamePart("N");
        Assertions.assertEquals(2, tags.size(), "returns two tags with N in them");
    }

    @Test
    public void serviceGetsTagById() {
        Tag tag = TestUtility.createTag();
        Tag savedTag = tagRepository.store(tag);
        Tag serviceTag = tagService.getTag(savedTag.getId());
        Assertions.assertEquals(savedTag, serviceTag, "two objects must match");
    }
}
