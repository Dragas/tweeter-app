import {Component, Input, OnInit} from '@angular/core';
import {TweetResponse} from "../api/models/tweet-response";

@Component({
  selector: 'app-tweet-component',
  templateUrl: './tweet-component.component.html',
  styleUrls: ['./tweet-component.component.scss']
})
export class TweetComponentComponent implements OnInit {

  @Input()
  tweet: TweetResponse;

  constructor() {
    this.tweet = {};
  }

  ngOnInit(): void {
  }

}
