import {Component, OnInit} from '@angular/core';
import {TweetControllerService} from "../api/services/tweet-controller.service";
import {RegisterUserRequest} from "../api/models/register-user-request";
import {UserService} from "../user.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  private api: TweetControllerService;
  registerRequest: RegisterUserRequest;
  private userService: UserService;
  private router: Router;

  constructor(api: TweetControllerService, router: Router, userService: UserService) {
    this.api = api;
    this.registerRequest = {};
    this.router = router;
    this.userService = userService;
  }

  ngOnInit(): void {
  }

  onSubmit($event: Event) {
    this.api.registerUserUsingPost({
      body: this.registerRequest
    }).subscribe((it) => {
      this.api.loginUsingPost({
        body: {
          username: this.registerRequest.loginId,
          password: this.registerRequest.password
        }
      }).subscribe((token) => {
        this.userService.setToken(token);
        this.router.navigate([""])
      })
    })
  }
}
