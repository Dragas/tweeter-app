/* tslint:disable */
/* eslint-disable */
export interface LoginUserRequest {
  password?: string;
  username?: string;
}
