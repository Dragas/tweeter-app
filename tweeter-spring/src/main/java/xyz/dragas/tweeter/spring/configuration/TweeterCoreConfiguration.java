package xyz.dragas.tweeter.spring.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDatabaseFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoClientDatabaseFactory;
import xyz.dragas.tweeter.api.repository.TagRepository;
import xyz.dragas.tweeter.api.repository.TweetRepository;
import xyz.dragas.tweeter.api.repository.UserRepository;
import xyz.dragas.tweeter.api.service.TagService;
import xyz.dragas.tweeter.api.service.TweetService;
import xyz.dragas.tweeter.api.service.UserService;
import xyz.dragas.tweeter.core.service.DefaultTagService;
import xyz.dragas.tweeter.core.service.DefaultTweetService;
import xyz.dragas.tweeter.core.service.DefaultUserService;
import xyz.dragas.tweeter.mongo.MongoTagRepository;
import xyz.dragas.tweeter.mongo.MongoTweetRepository;
import xyz.dragas.tweeter.mongo.MongoUserRepository;
import xyz.dragas.tweeter.spring.mapper.TweetMapper;
import xyz.dragas.tweeter.spring.mapper.UserMapper;
import xyz.dragas.tweeter.spring.security.AuthenticationRepository;
import xyz.dragas.tweeter.spring.security.MongoAuthenticationRepository;

import java.time.Clock;

@Configuration
public class TweeterCoreConfiguration {

    @Bean
    public TagService tagService(TagRepository tagRepository) {
        return new DefaultTagService(tagRepository);
    }

    @Bean
    public TagRepository tagRepository(MongoTemplate mongoClient) {
        return new MongoTagRepository(mongoClient);
    }

    @Bean
    public UserRepository userRepository(MongoTemplate mongoClient) {
        return new MongoUserRepository(mongoClient);
    }

    @Bean
    public UserService userService(UserRepository userRepository) {
        return new DefaultUserService(userRepository);
    }

    @Bean
    public TweetRepository tweetRepository(MongoTemplate mongoClient) {
        return new MongoTweetRepository(mongoClient);
    }

    @Bean
    public Clock clock() {
        return Clock.systemDefaultZone();
    }

    @Bean
    public TweetService tweetService(TweetRepository tweetRepository) {
        return new DefaultTweetService(tweetRepository);
    }

    @Bean
    public UserMapper userMapper() {
        return new UserMapper();
    }
    @Bean
    public TweetMapper tweetMapper() {
        return new TweetMapper();
    }

    @Bean
    public MongoTemplate mongoTemplate(MongoDatabaseFactory mongoDatabaseFactory) {
        return new MongoTemplate(mongoDatabaseFactory);
    }

/*    @Bean
    public MongoDatabaseFactory factory(@Value("spring.data.mongodb.uri") String mongodburi) {
        return new SimpleMongoClientDatabaseFactory(mongodburi);
    }*/

    @Bean
    public AuthenticationRepository authenticationRepository(MongoTemplate mongoTemplate) {
        return new MongoAuthenticationRepository(mongoTemplate);
    }

}
