#!/usr/bin/env bash

echo "127.0.0.1 grafana.infra" >> /etc/hosts
echo "127.0.0.1 kibana.infra" >> /etc/hosts
echo "127.0.0.1 prometheus.infra" >> /etc/hosts
echo "127.0.0.1 tomcat-tweeter-app.infra" >> /etc/hosts
echo "127.0.0.1 zookeeper.infra" >> /etc/hosts
