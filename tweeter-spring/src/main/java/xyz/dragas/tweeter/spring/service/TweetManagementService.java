package xyz.dragas.tweeter.spring.service;

import org.springframework.security.core.userdetails.UserDetails;
import xyz.dragas.tweeter.spring.model.TweetRequest;
import xyz.dragas.tweeter.spring.model.TweetResponse;

import java.util.Collection;

public interface TweetManagementService {
    Collection<TweetResponse> getTweets();
    Collection<TweetResponse> getTweets(String username);
    TweetResponse createTweet(UserDetails details, TweetRequest request);
    TweetResponse updateTweet(UserDetails details, String id, String content);
    TweetResponse deleteTweet(UserDetails details, String id);
    TweetResponse likeTweet(UserDetails details, String id);
    TweetResponse replyTweet(UserDetails details, String id, TweetRequest request);
}
