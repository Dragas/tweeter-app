import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {ApiModule} from "./api/api.module";
import { LoginComponent } from './login/login.component';
import {FormsModule} from "@angular/forms";
import { TweetComponentComponent } from './tweet-component/tweet-component.component';
import { TweetCreateComponentComponent } from './tweet-create-component/tweet-create-component.component';
import {HttpClientModule} from "@angular/common/http";
import { MainComponent } from './main/main.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { MainbarComponent } from './mainbar/mainbar.component';
import { RegisterComponent } from './register/register.component';
import { TweetReplyComponent } from './tweet-reply/tweet-reply.component';
import { LogoutComponent } from './logout/logout.component';
import { ErrorComponent } from './error/error.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    TweetComponentComponent,
    TweetCreateComponentComponent,
    MainComponent,
    NotFoundComponent,
    MainbarComponent,
    RegisterComponent,
    TweetReplyComponent,
    LogoutComponent,
    ErrorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ApiModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
