import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import { Observable } from 'rxjs';
import {UserService} from "./user.service";

@Injectable({
  providedIn: 'root'
})
export class LoginGuardGuard implements CanActivate {
  private userService: UserService;
  private router: Router;
  constructor(userService: UserService, router: Router) {
    this.userService = userService;
    this.router = router;
  }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if(!this.userService.isLoggedIn()) {
      return this.router.parseUrl("/login");
    }
    return true;
  }

}
