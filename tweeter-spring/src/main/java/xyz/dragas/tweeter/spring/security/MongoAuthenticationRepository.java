package xyz.dragas.tweeter.spring.security;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.UUID;

public class MongoAuthenticationRepository implements AuthenticationRepository {
    private static final GrantedAuthority USER = new GrantedAuthority() {
        @Override
        public String getAuthority() {
            return "USER";
        }
    };
    private final MongoTemplate mongoClient;

    public MongoAuthenticationRepository(MongoTemplate mongoTemplate) {
        this.mongoClient = mongoTemplate;
    }

    @Override
    public UserDetails getUserDetails(String username) {
        String token = getTokenForUser(username);
        UserDetails details = getUserByToken(token);
        if(details == null) {
            details = createUserDetails(username);
        }
        else {
            logout(details);
        }
        TokenWrapper wrapper = new TokenWrapper();
        token = UUID.randomUUID().toString();
        wrapper.setToken(token);
        wrapper.setUsername(username);
        mongoClient.insert(wrapper, "authentications");
        return details;
    }

    @Override
    public String getTokenForUser(String username) {
        return Optional.ofNullable(mongoClient.findOne(Query.query(Criteria.where("username").is(username)), TokenWrapper.class, "authentications")).map(TokenWrapper::getToken).orElse("");
    }

    @Override
    public UserDetails getUserByToken(String uuid) {
        TokenWrapper wrapper = mongoClient.findOne(Query.query(Criteria.where("token").is(uuid)), TokenWrapper.class, "authentications");
        if(wrapper == null) {
            return null;
        }
        return createUserDetails(wrapper.getUsername());
    }

    private UserDetails createUserDetails(String username) {
        return new UserDetails() {
            @Override
            public Collection<? extends GrantedAuthority> getAuthorities() {
                return Collections.singleton(USER);
            }

            @Override
            public String getPassword() {
                return "";
            }

            @Override
            public String getUsername() {
                return username;
            }

            @Override
            public boolean isAccountNonExpired() {
                return true;
            }

            @Override
            public boolean isAccountNonLocked() {
                return true;
            }

            @Override
            public boolean isCredentialsNonExpired() {
                return true;
            }

            @Override
            public boolean isEnabled() {
                return true;
            }
        };
    }

    @Override
    public void logout(UserDetails user) {
        mongoClient.findAndRemove(Query.query(Criteria.where("username").is(user.getUsername())), TokenWrapper.class, "authentications");
    }

    private static class TokenWrapper {
        private String token;
        private String username;

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getUsername() {
            return username;
        }
    }
}
