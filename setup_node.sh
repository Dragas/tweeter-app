#!/usr/bin/env bash

yarn global add @angular/cli
PATH="$PATH:~/.yarn/bin"
yarn global add ng-openapi-gen